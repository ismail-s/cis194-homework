# CIS 194 homework

This is my attempt at doing the
[cis 194 homework](https://www.seas.upenn.edu/~cis194/spring13/lectures.html)
as part of my attempts to learn haskell. Do NOT look at the code in this repo
if you are working throught the exercises yourself. Instead, you probably should
try and solve them yourself, and then take a look and see how I did compared to
you.

The main reason for me doing these exercises like this is so that I can use
[Cloud 9](https://c9.io), which is an awesome service for programming anywhere.