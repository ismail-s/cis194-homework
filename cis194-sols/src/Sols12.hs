{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Sols12 where

import Control.Monad.Random
import Control.Monad (replicateM)
import Data.List (sortBy)

------------------------------------------------------------
-- Die values

newtype DieValue = DV { unDV :: Int } 
  deriving (Eq, Ord, Show, Num)

first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

instance Random DieValue where
  random           = first DV . randomR (1,6)
  randomR (low,hi) = first DV . randomR (max 1 (unDV low), min 6 (unDV hi))

die :: Rand StdGen DieValue
die = getRandom

------------------------------------------------------------
-- Risk

type Army = Int

data Battlefield = Battlefield { attackers :: Army, defenders :: Army }
    deriving Show

-- |Simulate a single battle in Risk.
battle :: Battlefield -> Rand StdGen Battlefield
battle Battlefield {attackers=a, defenders = d} =
    let attackerDice = if a < 4 then a - 1 else 3
        defenderDice = if d < 3 then d else 2
        attackerRolls = replicateM attackerDice die
        defenderRolls = replicateM defenderDice die
        sortDesc = sortBy (flip compare)
        f (a, d) = if a > d then (0, -1) else (-1, 0)
    in attackerRolls >>= \aa ->
       defenderRolls >>= \dd ->
        let scores = zipWith (curry f) (sortDesc aa) (sortDesc dd)
            newAttackerScore =a + sum (map fst scores)
            newDefenderScore = d + sum (map snd scores)
        in return $ Battlefield newAttackerScore newDefenderScore

-- |Simulate trying to invade the defending territory
invade :: Battlefield -> Rand StdGen Battlefield
invade b =
    battle b >>= \b'@(Battlefield a' d') ->
        if d' < 1 || a' < 2
        then return b'
        else invade b'

-- |Run 1000 simulations of trying to invade the defending territory, and
-- return the ratio of success to failures
successProb :: Battlefield -> Rand StdGen Double
successProb b =
    replicateM 1000 (invade b) >>= \bs ->
        let
            wins = (length . filter (==True) . map ((>1) . attackers)) bs
        in return (realToFrac wins / 1000)
