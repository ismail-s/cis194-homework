{-# OPTIONS_GHC -Wall #-}
module Sols3 where

import Data.List (transpose)
import Data.Char (isSpace)
import Text.Printf (printf)

skips :: [a] -> [[a]]
skips l = map (every l) [1..length l] where
    every l n = case drop (n-1) l of
        [] -> []
        (x:xs) -> x:every xs n

localMaxima :: [Integer] -> [Integer]
localMaxima (a:b:c:ds) = if b>a && b>c then
    b:localMaxima (c:ds) else
    localMaxima (b:c:ds)
localMaxima _ = []

histogram :: [Integer] -> String
histogram [] = "\n==========\n0123456789\n"
histogram l = "\n" ++ foldr insert "" l ++ histogram []  where
    [s, t] = " *"
    insert :: Integer -> String -> String
    insert n "" = replicate (fromInteger n) s ++ t:replicate (9-fromInteger n) s
    insert n s = let n' = fromInteger n
                     w = (transpose . lines) s
                     x = t:dropWhile isSpace (w !! n')
                     w' = take n' w ++ x:drop (n'+1) w
                     maxLength = maximum $ map length w'
                     w'' = map (printf ("%" ++ show maxLength ++ "s")) w'
                 in (init . unlines . transpose) w''
