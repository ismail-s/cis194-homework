{-# LANGUAGE GeneralizedNewtypeDeriving, FlexibleInstances #-}
{-# OPTIONS_GHC -Wall #-}
module Sols7 where

import Data.Monoid
import Data.Char (toLower)
import Sols7Files.Sized
import Sols7Files.Buffer
import Sols7Files.Editor (runEditor, editor)

-- |A tree structure with annotations at each node.
-- The annotations should be monoids. This tree structure
-- is designed for fast appends.
data JoinList m a = Empty
                  | Single m a
                  | Append m (JoinList m a) (JoinList m a)
    deriving (Eq, Show)

-- |Get the root annotation of a JoinList
tag :: Monoid m => JoinList m a -> m
tag Empty = mempty
tag (Single m _) = m
tag (Append m _ _) = m

-- |Join 2 JoinLists together, also joining their annotations together
-- in the appropriate manner.
(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a
x +++ Empty = x
Empty +++ x = x
a +++ b = Append (tag a <> tag b) a b

-- |Return the element at the given index, or Nothing if the index
-- is out of bounds. The annotations on the JoinList are used
-- to speed up the lookup.
indexJ :: (Sized b, Monoid b) => Int -> JoinList b a -> Maybe a
indexJ _ Empty = Nothing
indexJ 0 (Single _ a) = Just a
indexJ _ (Single _ _) = Nothing
indexJ n _ | n < 0 = Nothing
indexJ n (Append b _ _) | n >= (getSize . size) b = Nothing
indexJ n (Append _ x y) = if n < e
    then indexJ n x
    else indexJ (n - e) y
    where e = (getSize . size . tag) x

-- |Drop n elements from a JoinList. analogoous to 'drop'.
dropJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
dropJ x a | x < 1 = a
dropJ x a | x >= (getSize . size . tag) a = Empty
dropJ _ Empty = Empty
dropJ _ (Single _ _) = Empty
dropJ 1 (Append _ a a') = if e > 0
    then dropJ 1 a +++ a'
    else a +++ dropJ 1 a'
    where e = (getSize . size . tag) a
dropJ x (Append _ a a') = dropJ (x-1) $ if e > 0
    then dropJ 1 a +++ a'
    else a +++ dropJ 1 a'
    where e = (getSize . size . tag) a

-- | Take 'n' elements from a JoinList. Analogous to 'take'.
takeJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
takeJ x _ | x < 1 = Empty
takeJ x a | x >= (getSize . size . tag) a = a
takeJ _ Empty = Empty
takeJ _ a@(Single _ _) = a
takeJ 1 (Append _ a a') = if e > 0
    then takeJ 1 a
    else takeJ 1 a'
    where e = (getSize . size . tag) a
takeJ x l@(Append _ a a') = if e > 0
    then takeJ 1 a +++ takeJ (x-1) (dropJ 1 l)
    else takeJ 1 a' +++ takeJ (x-1) (dropJ 1 l)
    where e = (getSize . size . tag) a


-- |Holds a scrabble score
newtype Score = Score Int deriving (Eq, Ord, Show, Num)

instance Monoid Score where
    mempty = Score 0
    mappend = (+)

-- |returns the scrabble score for the given character
score :: Char ->Score
score c | c `elem` ['A'..'Z'] = score (toLower c)
score c | c `elem` "aeilnorstu" = Score 1
score c | c `elem` "dg" = Score 2
score c | c `elem` "bcmp" = Score 3
score c | c `elem` "fhvwy" = Score 4
score c | c == 'k' = Score 5
score c | c `elem` "jx" = Score 8
score c | c `elem` "qz" = Score 10
score _ = mempty

-- |returns the scrabble score for the given string
scoreString :: String -> Score
scoreString = mconcat . map score

-- |returns a JoinList of the input string with its
-- scrabble score as an annotation
scoreLine :: String -> JoinList Score String
scoreLine s = Single (scoreString s) s

removeNewlines :: String -> String
removeNewlines = removeStartNewlines . removeEndNewlines where
    removeEndNewlines "" = ""
    removeEndNewlines s = if last s == '\n' then removeEndNewlines (init s) else s
    removeStartNewlines "" = ""
    removeStartNewlines (x:xs) | x == '\n' = removeStartNewlines xs
    removeStartNewlines s = s

instance Buffer (JoinList (Score, Size) String) where
    toString Empty = ""
    toString (Single _ a) = a
    toString (Append _ a b) = toString a ++ '\n':toString b

    fromString "" = Empty
    fromString s | length (lines s) == 1 = Single (scoreString s, Size 1) (removeNewlines s)
    fromString s = (foldr1 (+++) . map fromString . lines) s

    line = indexJ

    replaceLine _ _ Empty = Empty
    replaceLine 0 s (Single _ _) = fromString s
    replaceLine _ _ x@(Single _ _) = x
    replaceLine n s j = if numLines b == jn || numLines a == jn then j
        else a +++ fromString s +++ b where
        jn = numLines j
        a = takeJ n j
        b = dropJ (n+1) j

    numLines Empty = 0
    numLines (Single (_, Size x) _) = x
    numLines (Append (_, Size x) _ _) = x

    value Empty = 0
    value (Single (Score x, _) _) = x
    value (Append (Score x, _) _ _) = x

-- |Run the editor using our JoinList as the
-- way of storing the buffer in memory
runJoinListEditor :: IO ()
runJoinListEditor = runEditor editor (fromString s :: JoinList (Score, Size) String)
    where s = "" ++
              "This buffer is for notes you don't want to save, and for" ++
              "evaluation of steam valve coefficients." ++
              "To load a different file, type the character L followed" ++
              "by the name of the file."
