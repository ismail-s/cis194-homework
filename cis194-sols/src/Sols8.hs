module Sols8 where

import Data.Tree
import Data.List (sort)

-- |Employee names are represented by Strings.
type Name = String

-- |The amount of fun an employee would have at the party, represented
-- by an Integer
type Fun = Integer

-- |An Employee consists of a name and a fun score.
data Employee = Emp { empName :: Name, empFun :: Fun }
  deriving (Show, Read, Eq)

-- |A small company hierarchy to use for testing purposes.
testCompany :: Tree Employee
testCompany
  = Node (Emp "Stan" 9)
    [ Node (Emp "Bob" 2)
      [ Node (Emp "Joe" 5)
        [ Node (Emp "John" 1) []
        , Node (Emp "Sue" 5) []
        ]
      , Node (Emp "Fred" 3) []
      ]
    , Node (Emp "Sarah" 17)
      [ Node (Emp "Sam" 4) []
      ]
    ]

testCompany2 :: Tree Employee
testCompany2
  = Node (Emp "Stan" 9)
    [ Node (Emp "Bob" 3) -- (8, 8)
      [ Node (Emp "Joe" 5) -- (5, 6)
        [ Node (Emp "John" 1) [] -- (1, 0)
        , Node (Emp "Sue" 5) [] -- (5, 0)
        ]
      , Node (Emp "Fred" 3) [] -- (3, 0)
      ]
    , Node (Emp "Sarah" 17) -- (17, 4)
      [ Node (Emp "Sam" 4) [] -- (4, 0)
      ]
    ]

-- |A type to store a list of guests and their total fun score.
data GuestList = GL [Employee] Fun
  deriving (Show, Eq, Read)

instance Ord GuestList where
  compare (GL _ f1) (GL _ f2) = compare f1 f2

---------------------------------------------------------------

-- |Add an Employee to a GuestList, naively adding their
-- fun score to the existing one.
glCons :: Employee -> GuestList -> GuestList
glCons e@(Emp _ f) (GL es f') = GL (e:es) (f+f')

instance Monoid GuestList where
    mempty = GL [] 0
    (GL a f) `mappend` (GL a' f') = GL (a ++ a') (f + f')

-- |Return the GuestList that has the higher fun score.
-- If equal, the first one is returned.
moreFun :: GuestList -> GuestList -> GuestList
moreFun a b = if fun a < fun b
    then b else a
    where fun (GL _ f) = f

-- |Like 'foldr' / 'foldl', but for 'Tree'
treeFold :: (a -> [b] -> b) -> Tree a -> b
treeFold f (Node n l) = f n (map (treeFold f) l)

nextLevel :: Employee -> [(GuestList, GuestList)] -> (GuestList, GuestList)
nextLevel b l = (withBoss, withoutBoss) where
  withBoss = (glCons b . mconcat . map snd) l
  withoutBoss = (mconcat . map fst) l

maxFun :: Tree Employee -> GuestList
maxFun t = uncurry moreFun $ treeFold nextLevel t

wholeCompany :: IO ()
wholeCompany = readFile "src/Sols8Files/company.txt" >>= f
    where f s = putStrLn totalFun >> putStrLn sortedCompanyNames where
                    (GL empList fun) = maxFun $ read s
                    totalFun = "Total fun: " ++ show fun
                    sortedCompanyNames = (unlines . sort . map empName) empList
