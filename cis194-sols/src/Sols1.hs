module Sols1 where

import Data.Char (digitToInt)

toDigits :: Integer -> [Integer]
toDigits 0 = []
toDigits n
    | n < 0 = []
    | n < 10 = [n]
    | otherwise = map (toInteger . digitToInt) $ show n

toDigitsRev :: Integer -> [Integer]
toDigitsRev = reverse . toDigits

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther lst = d (reverse lst) [] False where
    d [] l _ = l
    d (x:xs) l True = d xs ((x*2):l) False
    d (x:xs) l False = d xs (x:l) True

sumDigits :: [Integer] -> Integer
sumDigits = foldr (\a b -> b + sum (toDigits a)) 0

validate :: Integer -> Bool
validate = (==0) . (`rem` 10) . sumDigits . doubleEveryOther . toDigits

type Peg = String
type Move = (Peg, Peg)

hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi 0 _ _ _ = []
hanoi 1 a b _ = [(a, b)]
hanoi n a b c = hanoi (n-1) a c b ++ (a, b):hanoi (n-1) c b a

hanoi4 :: Integer -> Peg -> Peg -> Peg -> Peg -> [Move]
hanoi4 0 _ _ _ _ = []
hanoi4 1 a b _ _ = [(a, b)]
hanoi4 2 a b c _ = [(a, c), (a, b), (c, b)]
hanoi4 3 a b c d = [(a, d), (a, c), (a, b), (c, b), (d, b)]
hanoi4 n a b c d = hanoi4 k a c d b ++ hanoi (n-k) a b d ++ hanoi4 k c b d a where
    k = n+1 - round (sqrt $ fromInteger $ 2*n+1)
