{-# OPTIONS_GHC -Wall #-}
module Sols4 where

import Data.List (sort)

fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x:xs)
    | even x = (x-2) * fun1 xs
    | otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' = product . map (subtract 2) . filter even

fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n | even n = n + fun2 (n `div` 2)
       | otherwise = fun2 (3 * n + 1)

fun2' :: Integer -> Integer
fun2' = sum . filter even . takeWhile (>1) . iterate g where
    g x | even x = x `div` 2
        | otherwise = 3*x+1

data Tree a = Leaf | Node Integer (Tree a) a (Tree a) deriving (Show)

foldTree :: [a] -> Tree a
foldTree = foldr g Leaf where
    height Leaf = -1
    height (Node x _ _ _) = x
    g x Leaf = Node 0 Leaf x Leaf
    g x (Node n t y t') = case compare (height t) (height t') of
        GT -> Node n t y (g x t')
        LT -> Node n (g x t) y t'
        EQ | height (g x t) == height t -> Node n (g x t) y t'
           | height (g x t') == height t' -> Node n t y (g x t')
           | otherwise -> Node (n+1) (g x t) y t'

xor :: [Bool] -> Bool
xor = foldr g False where
    g True False = True
    g True True = False
    g False b = b

map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\a b -> f a:b) []

cartProd :: [a] -> [b] -> [(a, b)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]

sieveSundaram :: Integer -> [Integer]
sieveSundaram n = (sort . map (\x->x*2+1) . filter (not . flip elem lst)) [1..n]
    where
        lst = filter (<=n) . map (\(a,b) -> a+b+2*a*b) $ cartProd [1..n] [1..n]
