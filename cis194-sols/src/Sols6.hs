{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}
module Sols6 where

fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

fibs1 :: [Integer]
fibs1 = map fib [0..]

fibs2 :: [Integer]
fibs2 = 0:1:map (\n -> fibs2 !! n + fibs2 !! (n+1)) [0..]

data Stream a = Stream a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Stream x s) = x:streamToList s

instance Show a => Show (Stream a) where
    show x = show $ take 20 $ streamToList x

streamRepeat :: a -> Stream a
streamRepeat x = Stream x (streamRepeat x)

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Stream a s) = Stream (f a) (streamMap f s)

streamFromSeed :: (a -> a) -> a -> Stream a
streamFromSeed f a = Stream a (streamFromSeed f (f a))

nats :: Stream Integer
nats = streamFromSeed (+1) 0

interleaveStreams :: Stream a -> Stream a -> Stream a
interleaveStreams (Stream a s) s' = Stream a (interleaveStreams s' s)

ruler :: Stream Integer
ruler = f 0 where
    f n = interleaveStreams (streamRepeat n) (f (n+1))


x :: Stream Integer
x = Stream 0 $ Stream 1 $ streamRepeat 0

instance Num (Stream Integer) where
    fromInteger n = Stream n $ streamRepeat 0
    negate (Stream a s) = Stream (negate a) (negate s)
    (Stream a s) + (Stream b s') = Stream (a + b) (s + s')
    (Stream x s) * b@(Stream y s') = Stream (x * y) $ streamMap (*x) s' + (s * b)

instance Fractional (Stream Integer) where
    a@(Stream x s) / b@(Stream y s') = Stream (div x y) $ streamMap (`div` y) $ s - ((a/b)*s')

fibs3 :: Stream Integer
fibs3 = x / (1-x-(x^2))


data Matrix = Matrix Integer Integer Integer Integer deriving Show

instance Num Matrix where
    negate (Matrix a b c d) = Matrix (-a) (-b) (-c) (-d)
    (Matrix a b c d) + (Matrix a' b' c' d') = Matrix (a+a') (b+b') (c+c') (d+d')
    (Matrix a b c d) * (Matrix a' b' c' d') = Matrix (a*a'+b*c') (a*b'+b*d') (c*a'+d*c') (c*b'+d*d')

fib4 :: Integer -> Integer
fib4 0 = 0
fib4 1 = 1
fib4 n = getRes $ Matrix 1 1 1 0 ^ (n-1)
    where getRes (Matrix x _ _ _) = x
