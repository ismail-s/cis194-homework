{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wall #-}

module Sols2 where

import Data.FileEmbed (embedStringFile)
import Data.String ()
import Text.Read (readMaybe)

import Control.Applicative ()

sampleFile :: String
sampleFile = $(embedStringFile "src/files/sample.log")

errorFile :: String
errorFile = $(embedStringFile "src/files/error.log")

data MessageType = Info
                 | Warning
                 | Error Int
  deriving (Show, Eq)

type TimeStamp = Int

data LogMessage = LogMessage MessageType TimeStamp String
                | Unknown String
  deriving (Show, Eq)

data MessageTree = Leaf
                 | Node MessageTree LogMessage MessageTree
  deriving (Show, Eq)

-- | @testParse p n f@ tests the log file parser @p@ by running it
--   on the first @n@ lines of file @f@.
testParse :: (String -> [LogMessage])
          -> Int
          -> String
          -> [LogMessage]
testParse parse n = take n . parse

-- | @testWhatWentWrong p w f@ tests the log file parser @p@ and
--   warning message extractor @w@ by running them on the log file
--   @f@.
testWhatWentWrong :: (String -> [LogMessage])
                  -> ([LogMessage] -> [String])
                  -> String
                  -> [String]
testWhatWentWrong parse whatWentWrong = whatWentWrong . parse

parseMessage :: String -> LogMessage
parseMessage s = let l = words s
                     handleMaybe (Just x) = x
                     handleMaybe Nothing = Unknown s
                 in if length l < 3 then Unknown s
                    else case l of
                      ("I":ls) -> handleMaybe $ do
                        n <- readMaybe (head ls)
                        return $ LogMessage Info n $ unwords $ tail ls
                      ("W":ls) -> handleMaybe $ do
                        n <- readMaybe (head ls)
                        return $ LogMessage Warning n $ unwords $ tail ls
                      ("E":t:u:ls) -> handleMaybe $ do
                        t' <- readMaybe t
                        u' <- readMaybe u
                        return $ LogMessage (Error t') u' $ unwords ls
                      _ -> Unknown s

parse :: String -> [LogMessage]
parse = (map parseMessage) . lines

insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) m = m
insert l Leaf = Node Leaf l Leaf
insert l@(LogMessage _ t _) (Node Leaf l'@(LogMessage _ t' _) m) = case compare t t' of
    GT -> Node Leaf l' (insert l m)
    _ -> Node (Node Leaf l Leaf) l' m
insert l@(LogMessage _ t _) (Node m l'@(LogMessage _ t' _) Leaf) = case compare t t' of
    GT -> Node m l' (Node Leaf l Leaf)
    _ -> Node (insert l m) l' Leaf
insert l@(LogMessage _ t _) (Node m l'@(LogMessage _ t' _) m') = case compare t t' of
    GT -> Node m l' (insert l m')
    _ -> Node (insert l m) l' m'

build :: [LogMessage] -> MessageTree
build = foldr insert Leaf

inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node Leaf l Leaf) = [l]
inOrder (Node m l m') = inOrder m ++ [l] ++ inOrder m'

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong l = (map showLogMsg . inOrder . build . filter f) l where
    f (LogMessage (Error n) _ _) = n >= 50
    f _ = False
    showLogMsg (LogMessage _ _ s) = s
