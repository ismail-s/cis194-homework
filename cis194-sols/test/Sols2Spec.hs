module Sols2Spec where

import Test.Hspec
import Sols2

spec = do
  describe "parseMessage" $ do
    it "can parse error messages" $
      parseMessage "E 2 562 help help" `shouldBe`LogMessage (Error 2) 562 "help help"
    it "can parse informational messages" $
      parseMessage "I 29 la la la" `shouldBe`LogMessage Info 29 "la la la"
    it "can handle badly formatted messages" $
      parseMessage "This is not in the right format" `shouldBe`Unknown "This is not in the right format"
  describe "parse" $
    it "can parse the sampleFile" $ do
      let res = [LogMessage Info 6 "Completed armadillo processing",LogMessage Info 1 "Nothing to report",LogMessage Info 4 "Everything normal",LogMessage Info 11 "Initiating self-destruct sequence",LogMessage (Error 70) 3 "Way too many pickles",LogMessage (Error 65) 8 "Bad pickle-flange interaction detected",LogMessage Warning 5 "Flange is due for a check-up",LogMessage Info 7 "Out for lunch, back in two time steps",LogMessage (Error 20) 2 "Too many pickles",LogMessage Info 9 "Back from lunch",LogMessage (Error 99) 10 "Flange failed!"]
      testParse parse 99 sampleFile `shouldBe` res
  describe "insert" $ do
    let l = LogMessage Info 6 "test"
        l' = LogMessage Info 7 "test"
        l'' = LogMessage Info 8 "test"
    it "can insert a LogMessage into an empty tree" $
      insert l Leaf `shouldBe` Node Leaf l Leaf
    it "can insert a LogMessage into a tree with a larger element" $
      insert l (Node Leaf l' Leaf) `shouldBe` Node (Node Leaf l Leaf) l' Leaf
    it "can insert a LogMessage into a tree with a smaller element" $
      insert l' (Node Leaf l Leaf) `shouldBe` Node Leaf l (Node Leaf l' Leaf)
    it "can insert a LogMessage into a tree with 3 elements in it" $ do
      let p = LogMessage Warning 2 "blah"
      insert p (Node (Node Leaf l Leaf) l' (Node Leaf l'' Leaf)) `shouldBe` Node (Node (Node Leaf p Leaf) l Leaf) l' (Node Leaf l'' Leaf)
  describe "build" $
    it "builds a MessageTree from a list of LogMessages" $ do
      let res = Node (Node (Node (Node Leaf (LogMessage Info 1 "Nothing to report") Leaf) (LogMessage (Error 20) 2 "Too many pickles") (Node (Node (Node Leaf (LogMessage (Error 70) 3 "Way too many pickles") (Node Leaf (LogMessage Info 4 "Everything normal") Leaf)) (LogMessage Warning 5 "Flange is due for a check-up") (Node Leaf (LogMessage Info 6 "Completed armadillo processing") Leaf)) (LogMessage Info 7 "Out for lunch, back in two time steps") (Node Leaf (LogMessage (Error 65) 8 "Bad pickle-flange interaction detected") Leaf))) (LogMessage Info 9 "Back from lunch") Leaf) (LogMessage (Error 99) 10 "Flange failed!") (Node Leaf (LogMessage Info 11 "Initiating self-destruct sequence") Leaf)
      (build . parse) sampleFile `shouldBe` res
  describe "inOrder" $
    it "turns a MessageTree into an ordered list of Logmessages" $ do
      let res = [LogMessage Info 1 "Nothing to report",LogMessage (Error 20) 2 "Too many pickles",LogMessage (Error 70) 3 "Way too many pickles",LogMessage Info 4 "Everything normal",LogMessage Warning 5 "Flange is due for a check-up",LogMessage Info 6 "Completed armadillo processing",LogMessage Info 7 "Out for lunch, back in two time steps",LogMessage (Error 65) 8 "Bad pickle-flange interaction detected",LogMessage Info 9 "Back from lunch",LogMessage (Error 99) 10 "Flange failed!",LogMessage Info 11 "Initiating self-destruct sequence"]
      (inOrder . build . parse) sampleFile `shouldBe` res
  describe "whatWentWrong" $
    it "finds error mesages with severity>=50, and returns them, sorted by timestamp" $
      testWhatWentWrong parse whatWentWrong sampleFile `shouldBe` ["Way too many pickles","Bad pickle-flange interaction detected","Flange failed!"]
