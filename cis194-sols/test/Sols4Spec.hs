module Sols4Spec where

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Modifiers
import Sols4

spec = do
    describe "fun1'" $
      it "behaves the same as fun1" $
        property $ \l -> fun1 l == fun1' l
    describe "fun2'" $
      it "behaves the same as fun2" $
        property $ \(Positive n) -> fun2 n == fun2' n
    describe "xor" $
      it "returns True iff there are an odd no. of True's in the input" $
        property $ \l -> xor l == (odd . length . filter (==True)) l
    describe "map'" $
      it "behaves the same as map" $
        property $ \n l -> map (*n) l == map' (*n) (l :: [Integer])
    describe "sieveSundaram" $
      it "generates the prime nos. (not including 2)" $
        sieveSundaram 100 `shouldBe` [3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199]
