module Sols12Spec where

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Modifiers
import Test.QuickCheck.Monadic
import Control.Monad.Random (evalRandIO)
import Sols12

spec = do
    describe "battle" $
      it "simulates a single battle in Risk" $
        property $ \(Positive x) (Positive dd) -> monadicIO $ do
          --make sure at least 2 attackers
          let aa = x + 1
          (Battlefield a d) <- run $ evalRandIO $ battle $ Battlefield aa dd
          assert (a <= aa)
          assert (d <= dd)
          assert (a < aa || d < dd)
    describe "invade" $
      it "repeatedly runs battle until one side can't continue fighting" $
        property $ \(Positive aa) (Positive dd) -> monadicIO $ do
          (Battlefield a d) <- run $ evalRandIO $ invade $ Battlefield aa dd
          assert (a == 1 || d == 0)
    describe "successProb" $
      it "runs 1000 simulations to guess the probability of success of invasion" $ do
        m <- evalRandIO $ successProb $ Battlefield 40 5
        m `shouldSatisfy` (> 0.75)
        n <- evalRandIO $ successProb $ Battlefield 5 40
        n `shouldSatisfy` (< 0.25)
