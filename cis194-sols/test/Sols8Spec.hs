module Sols8Spec where

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Modifiers
import Sols8
import Data.Tree

spec = do
    let a = GL [Emp "te" 5, Emp "xe" 1] 6
        b = GL [Emp "zw" 3, Emp "be" 2] 5
        c = GL [Emp "sdf" 6] 6
    describe "glCons" $
      it "adds an employee to a guestlist, updating the fun score" $
        glCons (Emp "blah" 1) a `shouldBe`
            GL [Emp "blah" 1, Emp "te" 5, Emp "xe" 1] 7
    describe "moreFun" $ do
      it "returns the GuestList with a higher fun score" $ do
        moreFun a b `shouldBe` a
        moreFun b a `shouldBe` a
      it "returns the first GuestList if they both have equal fun scores" $ do
        moreFun a c `shouldBe` a
        moreFun c a `shouldBe` c
    describe "treeFold" $
      it "is like foldr/foldl, but for a Tree" $
        property (\w x y z -> treeFold (\a l -> sum (a:l))
                    (Node w [Node x [], Node y [Node z []]]) == w+x+y+(z :: Integer))
    describe "maxFun" $
      it "returns the fun-maximising guestlist for a given employee tree" $ do
        maxFun testCompany `shouldBe` GL [Emp "Bob" 2, Emp "John" 1, Emp "Sue" 5, Emp "Sarah" 17] 25
        maxFun testCompany2 `shouldBe` GL [Emp "Bob" 3, Emp "John" 1, Emp "Sue" 5, Emp "Sarah" 17] 26
