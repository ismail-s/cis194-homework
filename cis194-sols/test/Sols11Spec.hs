module Sols11Spec where

import Data.Char (isUpper)
import Data.Maybe (isJust, isNothing)
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Modifiers
import Sols11
import Sols11Files.AParser

spec = do
    describe "zeroOrMore" $ do
      it "runs a parser as many times as possible, returning a list of the results" $
        property $ \(NonNegative a) (NonNegative b) ->
          let res1 = replicate a 'a'
              res2 = replicate b 'b' in
          runParser (zeroOrMore $ char 'a') (res1 ++ res2) == Just (res1, res2)
      it "never returns Nothing, always a Just" $
        property $ \s -> isJust $ runParser (zeroOrMore $ char 'a') s
    describe "oneOrMore" $ do
      it "runs a parser as many times as possible, returning a list of the results if the parser matches at least once" $
        property $ \(Positive a) (NonNegative b) ->
          let res1 = replicate a 'a'
              res2 = replicate b 'b' in
          runParser (oneOrMore $ char 'a') (res1 ++ res2) == Just (res1, res2)
      it "returns Nothing if the parser doesn't match" $
        property $ \(NonNegative a) ->
          isNothing $ runParser (oneOrMore $ char 'a') (replicate a 'b')
    describe "spaces" $
      it "parses zero or more spaces" $
        property $ \(Positive a) (NonNegative b) ->
          let res1 = replicate a ' '
              res2 = replicate b 'a' in
          runParser spaces (res1 ++ res2) == Just (res1, res2)
    describe "ident" $ do
      it "parses an alpha char, followed by zero or more alphanumeric chars" $ do
        runParser ident "foobar baz" `shouldBe` Just ("foobar", " baz")
        runParser ident "foo33fA" `shouldBe` Just ("foo33fA", "")
        runParser ident "f3453:@~" `shouldBe` Just ("f3453", ":@~")
      it "returns Nothing otherwise" $
        property $ \(NonNegative n) s -> let types = n :: Integer in
          isNothing $ runParser ident (show n ++ s)
    describe "parseSExpr" $ do
      it "can parse numbers" $
        property $ \(NonNegative n) -> runParser parseSExpr (show n) == Just (A (N n), "")
      it "can parse idents" $ do
        runParser parseSExpr "foobar baz" `shouldBe` Just (A (I "foobar"), "baz")
        runParser parseSExpr "foo33fA" `shouldBe` Just (A (I "foo33fA"), "")
        runParser parseSExpr "f3453:@~" `shouldBe` Just (A (I "f3453"), ":@~")
      it "can parse lists of numbers and idents enclosed in brackets" $
        runParser parseSExpr "(one two 3 4) randomjunk" `shouldBe` Just (Comb (map A [I "one", I "two", N 3, N 4]), "randomjunk")
      it "can parse S-expressions with leading & trailing spaces" $
        property $ \(NonNegative a) (NonNegative b) ->
          let s = replicate a ' ' ++ "(one two 3 4)" ++ replicate b ' ' in
          runParser parseSExpr s == Just (Comb (map A [I "one", I "two", N 3, N 4]), "")
      it "returns Nothing on malformed input" $ do
        runParser parseSExpr "" `shouldBe` Nothing
        runParser parseSExpr "(no close bracket" `shouldBe` Nothing
        runParser parseSExpr "{}:@~><?" `shouldBe` Nothing
