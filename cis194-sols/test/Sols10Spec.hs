module Sols10Spec where

import Data.Char (ord, chr)
import System.Random
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Modifiers
import Sols10

newtype Uppercase = Uppercase Char
  deriving (Eq, Show, Ord)

instance Bounded Uppercase where
  minBound = Uppercase 'A'
  maxBound = Uppercase 'Z'

instance Random Uppercase where
  randomR (Uppercase x, Uppercase y) g = (Uppercase (chr a), g') where (a, g') = randomR (ord x, ord y) g
  random g = (Uppercase (chr a), g') where (a, g') = randomR (ord 'A', ord 'Z') g

instance Arbitrary Uppercase where
  arbitrary = arbitraryBoundedRandom

spec = do
    describe "Parser" $ do
      it "is an instance of Functor" $
        property $ \(NonNegative a) b ->
          -- Check we can apply (*n) to a Parser Integer
          runParser (fmap (*b) posInt) (show a ++ "blah") == Just (a*b, "blah")
      it "is an instance of Applicative" $
        property $ \(NonNegative n) c ->
          runParser ((,) <$> char c <*> posInt) (c:show n) == Just ((c, n), "")
    describe "abParser" $
      it "checks if the first 2 chars are a & b and returns them" $
        property $ \a b s -> runParser abParser (a:b:s) == (
              if a == 'a' && b == 'b' then Just (('a', 'b'), s) else Nothing)
    describe "abParser_" $
      it "checks if the first 2 chars are a & b and returns ()" $
        property $ \a b s -> runParser abParser_ (a:b:s) == (
              if a == 'a' && b == 'b' then Just ((), s) else Nothing)
    describe "intPair" $
      it "checks if there are 2 nos. with a space inbetween and returns them" $
        property $ \(NonNegative a) (NonNegative b) ->
          runParser intPair (show a ++ ' ':show b) == Just ([a, b], "")
    describe "intOrUppercase" $ do
      it "parses integer values" $
        property $ \(NonNegative a) (Uppercase c) s -> let types = (a :: Integer) in
          runParser intOrUppercase (show a ++ c:s) == Just ((), c:s)
      it "parses uppercase chars" $
        property $ \(Uppercase a) (NonNegative n) s -> let types = (n :: Integer) in
          runParser intOrUppercase (a:(show n ++ s)) == Just ((), show n ++ s)
