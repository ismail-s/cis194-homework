module Sols5Spec where

import Test.Hspec
import Test.QuickCheck
import Sols5
import Sols5Files.Parser (parseExp)
import qualified Sols5Files.StackVM as S

spec = do
    describe "eval'" $ do
      it "evaluates ExprT expressions" $
        eval (Mul (Add (Lit 2) (Lit 3)) (Lit 4)) `shouldBe` 20
      it "just extracts the number out of a literal" $
        property $ \n -> eval (Lit n) == n
      it "can add 2 nums together" $
        property $ \a b -> eval (Add (Lit a) (Lit b)) `shouldBe` a + b
      it "can multiply 2 nums together" $
        property $ \a b -> eval (Mul (Lit a) (Lit b)) `shouldBe` a * b
    describe "evalStr" $ do
      it "evaluates strings, returning Just result or Nothing" $
        evalStr "2+(3*5)+3" `shouldBe` Just 20
      it "parses nums as a string, wrapping them in a Just" $
        property $ \n -> (evalStr . show) n == Just n
    describe "Expr" $ do
      it "is a typeclass with ExprT as an instance" $ do
        let expected = Mul (Add (Lit 2) (Lit 3)) (Lit 4)
        mul (add (lit 2) (lit 3)) (lit 4) `shouldBe` expected
      it "has Integer, Bool, MinMax and Mod7 as instances" $ do
        let testExp :: (Expr a) => Maybe a
            testExp = parseExp lit add mul "(3 * -4) + 5"
        testExp `shouldBe` (Just (-7) :: Maybe Integer)
        testExp `shouldBe` (Just True :: Maybe Bool)
        testExp `shouldBe` (Just (MinMax 5) :: Maybe MinMax)
        testExp `shouldBe` (Just (Mod7 0) :: Maybe Mod7)
    describe "compile" $
      it "parses and compiles a string into a program for StackVM" $
        compile "2+(3*5)" `shouldBe` Just [S.PushI 2, S.PushI 3, S.PushI 5, S.Mul, S.Add]
