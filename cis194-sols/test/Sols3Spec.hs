module Sols3Spec where

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Modifiers
import Sols3

spec = do
    describe "skips" $ do
      it "turns turns lists into a list of lists, each with the nth elements" $ do
        skips "ABCD" `shouldBe` ["ABCD", "BD", "C", "D"]
        skips "hello!" `shouldBe` ["hello!", "el!", "l!", "l", "o", "!"]
        skips [1] `shouldBe` [[1]]
        skips [True,False] `shouldBe` [[True,False], [False]]
        skips "" `shouldBe` []
      it "returns the list itself as the first element" $
        property $ \(NonEmpty l) -> (head . skips) l == (l :: String)

    describe "localMaxima" $ do
      it "finds the peaks if the list is drawn as a line graph" $ do
        localMaxima [2,9,5,6,1] `shouldBe` [9,6]
        localMaxima [2,3,4,1,5] `shouldBe` [4]
      it "returns an empty list if passed a strictly increasing list of nums" $
        property $ \n -> null (localMaxima [1..n])

    describe "histogram" $ do
      let e = '\n':replicate 10 '=' ++ "\n0123456789\n"
      it "prints an empty histogram when given an empty list" $
        histogram [] `shouldBe` e
      it "prints a correct histogram when given certain inputs" $ do
        histogram [1,1,1,5] `shouldBe` "\n *        \n\
                                         \ *        \n\
                                         \ *   *    " ++ e
        histogram [8,0,3,5,7,9,6,7,6] `shouldBe` "\n      **  \n\
                                                   \*  * *****" ++ e
      it "outputs the right no. of lines when given a repeated list" $
        property $ \(NonNegative n) -> (length . lines . histogram . replicate n) 5 == n+3
