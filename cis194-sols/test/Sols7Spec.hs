module Sols7Spec where

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Modifiers
import Sols7
import Sols7Files.Buffer
import Sols7Files.Sized
import Data.Maybe (isNothing)

spec = do
    let a = Single (Size 1) 'a'
        b = Append (Size 2) (Single (Size 1) 'b') (Single (Size 1) 'c')
        ab = a +++ b
    describe "+++" $
      it "joins JoinLists together" $ do
        let e = Empty :: JoinList Size Char
        e +++ e `shouldBe` e
        Empty +++ a `shouldBe` a
        a +++ Empty `shouldBe` a
        a +++ a `shouldBe` Append (Size 2) (Single (Size 1) 'a') (Single (Size 1) 'a')
        a +++ b `shouldBe` Append (Size 3) a b
    describe "indexJ" $ do
      it "returns Nothing when passed a negative index" $
        property $ \(Positive n) -> isNothing (indexJ (-n) b)
      it "returns Nothing when passed too big an index" $
        property $ \(Positive n) -> isNothing (indexJ (n+1) b)
      it "returns the item corresponding to the index" $
        map (`indexJ` ab) [0..2] `shouldBe` map Just "abc"
    describe "dropJ" $ do
      it "returns Empty when passed an int >= the size of the JoinList" $
        property $ \(Positive n) -> dropJ (n+2) ab == Empty
      it "returns the input when passed an int <=0" $
        property $ \(NonNegative n) -> dropJ (-n) ab == ab
      it "drops the given no. of elements otherwise" $ do
        dropJ 1 ab `shouldBe` b
        dropJ 2 ab `shouldBe` Single (Size 1) 'c'
    describe "takeJ" $ do
      it "returns Empty when passed an int <=0" $
        property $ \(NonNegative n) -> takeJ (-n) ab == Empty
      it "returns the input when passed an int >= the size of the JoinList" $
        property $ \(Positive n) -> takeJ (n+2) ab == ab
      it "takes the given no. of elements otherwise" $ do
        takeJ 1 ab `shouldBe` a
        takeJ 2 ab `shouldBe` Append (Size 2) (Single (Size 1) 'a') (Single (Size 1) 'b')
    describe "scoreString" $
      it "returns the Scrabble score of a string" $ do
        scoreString "yay " `shouldBe` Score 9
        scoreString "haskell!" `shouldBe` 14
    describe "scoreLine" $
      it "returns a JoinList of the input string with a scrabble score annotation" $
        scoreLine "yay " +++ scoreLine "haskell!"
            `shouldBe` Append (Score 23)
                              (Single (Score 9) "yay ")
                              (Single (Score 14) "haskell!")

    describe "toString/fromString" $
      it "converts between JoinList (Score, Size) String and String" $
        property $ \s ->
          toString (fromString s :: JoinList (Score, Size) String) ==
            (removeNewlines . unlines . filter (not . null) . lines) s
    describe "replaceLine" $ do
      let a = fromString "this\nis\na\ntest" :: JoinList (Score, Size) String
          as = toString a
      it "returns the input JoinList if passed an index <0" $
        property $ \(Positive n) -> toString (replaceLine (-n) "blah" a) == as
      it "returns the input JoinList if passed too big an index" $
        property $ \(Positive n) -> toString (replaceLine (n+3) "blah" a) == as
      it "replaces the element at the given index otherwise" $ do
        toString (replaceLine 0 "blah" a) `shouldBe` "blah\nis\na\ntest"
        toString (replaceLine 1 "blah" a) `shouldBe` "this\nblah\na\ntest"
        toString (replaceLine 2 "blah" a) `shouldBe` "this\nis\nblah\ntest"
        toString (replaceLine 3 "blah" a) `shouldBe` "this\nis\na\nblah"
