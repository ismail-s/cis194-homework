{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Sols1Spec (spec) where

import Test.Hspec
import Test.QuickCheck
import Sols1

newtype LessThan15 = LessThan15 Integer
  deriving (Eq, Num, Integral, Enum, Real, Ord, Show)

instance Bounded LessThan15 where
  minBound = 0
  maxBound = 15

instance Arbitrary LessThan15 where
  arbitrary = arbitraryBoundedIntegral
  shrink = shrinkIntegral

every :: Int -> [a] -> [a]
every n xs = case drop (n-1) xs of
              (y:ys) -> y:every n ys
              [] -> []

spec :: Spec
spec = do
  describe "toDigits" $ do
    it "turns nonnegative integers into a list of digits" $ do
      property $ \(NonNegative i) -> foldl (\b a->b*10+a) 0 (toDigits i) == i
    it "turns negative integers into an empty list" $ do
      property $ \(NonNegative i) -> toDigits (-i) == []

  describe "toDigitsRev" $ do
    it "turns nonnegative integers into a reversed list of digits" $ do
      property $ \(NonNegative i) -> foldr (\a b->b*10+a) 0 (toDigitsRev i) == i
    it "turns negative integers into an empty list" $ do
      property $ \(NonNegative i) -> toDigitsRev (-i) == []

  describe "doubleEveryOther" $ do
    it "doesn't double the odd digits from the right" $ do
      let f xs = (oddsFromRight . doubleEveryOther) xs == oddsFromRight xs where
            types = xs :: [Integer]
          oddsFromRight xs = every 2 (1:(reverse xs))
      property f
    it "doubles the even digits from the right" $ do
      let f xs = g xs == g' xs where
            types = xs :: [Integer]
            g = every 2 . reverse . doubleEveryOther
            g' = map (2*) . every 2 . reverse
      property f

  describe "sumDigits" $ do
    it "adds all the digits of all the numbers in a list together" $ do
      sumDigits [1, 2, 34, 567, 8, 910] `shouldBe` 46

  describe "validate" $ do
    it "checks to see if a credit card num is valid" $ do
      validate 4012888888881881 `shouldBe` True
      validate 4012888888881882 `shouldBe` False

  describe "hanoi" $ do
    it "can solve the towers of hanoi problem for 3 pegs" $ do
      hanoi 2 "a" "b" "c" `shouldBe` [("a","c"), ("a","b"), ("c","b")]
    it "gives solutions of length 2^n-1" $ do
      property $ \(LessThan15 n) -> fromIntegral (length (hanoi n "a" "b" "c")) == 2**(fromIntegral n)-1

  describe "hanoi4" $ do
    it "can solve the towers of hanoi problem for 4 pegs" $ do
      length (hanoi4 15 "a" "b" "c" "d") `shouldBe` 129
