module Sols6Spec where

import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Modifiers
import Sols6
import Data.List (transpose)

spec = do
    let first11Fibs = [0,1,1,2,3,5,8,13,21,34,55]
    describe "fib" $
      it "(badly) computes fibonacci nos." $ do
        fib 0 `shouldBe` 0
        fib 1 `shouldBe` 1
        fib 10 `shouldBe` 55
    describe "fibs1" $
      it "uses fib to create an infinite list of fib nos." $
        take 11 fibs1 `shouldBe` first11Fibs
    describe "fibs2" $
      it "Creates an infinite list of fib nos. using recursion and lazy evaluation" $
        take 11 fibs2 `shouldBe` first11Fibs

    describe "streamRepeat" $
      it "returns a stream of infinitely many copies of an element" $
        property $ \a -> show (streamRepeat a)
                      == show (replicate 20 (a :: Integer))
    describe "streamMap" $
      it "maps a function over a stream" $
        property $ \n -> show (streamMap (*2) (streamRepeat (n :: Integer)))
                      == show (replicate 20 (n*2))
    describe "streamFromSeed" $
      it "repeatedly applies a func to a seed to generate a Stream" $
        property $ \n -> show (streamFromSeed (*2) (n :: Integer))
                      == show (take 20 (iterate (*2) n))
    describe "interleaveStreams" $
      it "Creates a Stream by interleaving 2 Streams" $
        property $ \a b ->
              show (interleaveStreams (streamRepeat (a :: Integer)) (streamRepeat (b :: Integer)))
              == (show . concat . transpose) [replicate 10 a, replicate 10 b]
    describe "nats" $
      it "is a Stream of the natural nos. (including 0)" $
        property $ \n -> take n (streamToList nats) == take n [0..]
    describe "ruler" $
      it "is a Stream where the nth element is the largest power of 2 evenly dividing n" $
        take 16 (streamToList ruler) `shouldBe` [0,1,0,2,0,1,0,3,0,1,0,2,0,1,0,4]

    describe "fibs3" $
      it "returns the fibonacci nos. as a infinite Stream" $
        take 11 (streamToList fibs3) `shouldBe` first11Fibs
    describe "fib4" $
      it "computes the nth fibonacci no. very fast" $
        property $ \(NonNegative n) -> fib4 n + fib4 (n+1) == fib4 (n+2)
